#! /bin/bash
# @edt ASIX M01-ISO
# Curs 2019-2020
# funcions
# --------------------------------------------------------------------

function getAllUsersSize(){
	users=$(cut -d: -f1 /etc/passwd)
	for user in $users
	do
		echo "Usuari: $user"
		home=$(grep "^$user:" /etc/passwd | cut -d: -f6)
		getSize $home 2>> /dev/null
	done
	return 0
}


function getSizeIn(){
	while read -r line
	do
		getSize $line
	done
	return 0
}

function getSize(){
	homeDir=$1
	status=0
	grep ":$homeDir:[^:]*$" /etc/passwd &> /dev/null
	if [ $? -ne 0 ]
	then
		echo "$homeDir home dir inexistent"
		status=1
	else
		ocupacio=$(du -h $homeDir | tr '[:blank:]' ':' | cut -d: -f1 | tail -n1)
		echo "Ocupació: $ocupacio"
	fi
	return $status	
}

function getHoleList(){
	if [ $# -lt 1 ]
	then
		echo "Error: numero args no vàlid"
		echo "Usage:$0 login[...]"
		return 1
	fi
	logins=$*
	for login in $logins
	do
		grep "^$login:" /etc/passwd &> /dev/null
		if [ $? -ne 0 ]
		then
			echo "$login login inexistent"
		else
			getHome $login
		fi
	done
	return 0
}

function getHome(){
	login=$1
	status=0
	home=$(grep "^$login:" /etc/passwd | cut -d: -f6)
	echo $home
	ls $home &> /dev/null
	if [ $? -ne 0 ]
	then
		status=1
	fi
	return $status
}

function showPedidos(){
	oficina=$1
	if [ $# -ne 1 ]
	then
		echo "Error: numero d'args no vàlid"
		echo "Usage: $0 oficina"
		return 1
	fi
	if ! [ $oficina -eq 11 -o $oficina -eq 12 -o $oficina -eq 13 -o $oficina -eq 21 -o $oficina -eq 22 ]
	then
		echo "Error: $oficina oficina inexistent"
		echo "Usage: $0 oficina"
		return 2
	fi
	echo "OFICINA: $oficina"
	numEmpl=$(cat repventas.dat | tr '\t' ':' | grep "^[^:]*:[^:]*:[^:]*:$oficina:" | cut -d: -f1)
	for empl in $numEmpl
	do
		comandes=$(cat pedidos.dat | tr '\t' ':' | grep "^[^:]*:[^:]*:[^:]*:$empl:")
		echo "EMPLEAT: $empl"
		echo "COMANDES:"
		echo $comandes
	done	
	return 0
}

function showGidMembers2(){
	while read -r line
	do
		numUsuaris=$(grep "^[^:]*:[^:]*:[^:]*:$line:" /etc/passwd | wc -l)
		if [ $numUsuaris -ge 3 ]
		then
			infUsuaris=$(grep "^[^:]*:[^:]*:[^:]*:$line:" /etc/passwd | sort | cut -d: -f1,3,6)
			echo $infUsuaris
		fi
	done
	return 0
}

function showGidMembers(){
	while read -r line
	do
		infUsuaris=$(grep "^[^:]*:[^:]*:[^:]*:$line:" /etc/passwd | sort | cut -d: -f1,3,6)
		echo $infUsuaris
	done
	return 0
}

function showAllGroupMainMembers2(){
	llistaGids=$(sort /etc/group | cut -d: -f3)
	for gid in $llistaGids
	do
		totalUsuaris=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
		gname=$(grep ":$gid:[^:]*$" /etc/group | cut -d: -f1 | tr '[:lower:]' '[:upper:]')
		infUsuaris=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | cut -d: -f1,3,6,7 | sed -r 's/(^.*)/\t\1/')
		echo "$gname - Num Usurais: $totalUsuaris"
		echo "$infUsuaris"
	done
	return 0
}

function showAllGroupMainMembers(){
	llistaGids=$(sort /etc/group | cut -d: -f3)
	for gid in $llistaGids
	do
		gname=$(grep ":$gid:[^:]*$" /etc/group | cut -d: -f1)
		logins=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | sort | cut -d: -f1)
		echo "$gname"
		echo "$logins"
	done
	return 0
}
