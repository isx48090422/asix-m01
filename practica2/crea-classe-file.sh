#! /bin/bash
# @edt ASIX M01-ISO
# Curs 2019-2020
# 
# -------------------------------
BASEHOME=/home
classe=$1
# Mirar si existeix el grup de classe
grep "^$classe:" /etc/group &> /dev/null

if [ $? -ne 0 ]
then
        groupadd $classe
fi

# Crear directori de classe
mkdir $BASEHOME/$classe &> /dev/null && echo "mkdir done Ok"
chgrp $classe $BASEHOME/$classe && echo "chgrp OK"

# Crear usuaris
llistaAlumnes=$(cat $2)
for alumne in $llistaAlumnes
do
	user=$(echo $alumne | cut -d: -f1)
	password=$(echo $alumne | cut -d: -f2)
        useradd -m -n -g $classe -b $BASEHOME/$classe $user -p alumn &> /dev/null  && echo "user $user created Ok"
        echo $password | passwd $user --stdin
done
exit 0


