#! /bin/bash
# @edt ASIX M01-ISO
# Curs 2019-2020
# Rep un login i elimina TOT el que pertany a l’usuari, però deixant 
# un tar.gz de tots els fitxers que li pertanyen. Va generant una 
# traça per stderr de tot allò que va eliminant.
# --------------------------------------------------------------------

login=$1

home=$(grep "^$login:" /etc/passwd | cut -d: -f6)
tar -czvf /var/tmp/copia$login.tar.gz $home
rm -rfv $home >> /dev/stderr
userdel $login

exit 0
