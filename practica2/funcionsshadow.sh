#! /bin/bash
# @edt ASIX M01-ISO
# Curs 2019-2020
# funcions
# --------------------------------------------------------------------

function showShadowIn(){
	while read -r line
	do
		showShadow $line
	done
	return 0
}

function showShadowList(){
	if [ $# -lt 1 ]
	then
		echo "Error: número d'arguments invàlid"
		echo "Usage: $0 login[...]"
		return 1
	fi
	loginList=$*
	for login in $loginList
	do
		showShadow $login
		echo ""
	done
	return 0
}

function showShadow(){
	if [ $# -ne 1 ]
	then
		echo "Error: número d'arguments invàlid"
		echo "Usage: $0 login"
		return 1
	fi
	login=$1
	grep "^$login:" /etc/passwd &> /dev/null
	if [ $? -ne 0 ]
	then
		echo "Error: login $login inexistent"
		echo "Usage: $0 login"
		return 2
	fi
	password=$(grep "^$login:" /etc/shadow | cut -d: -f2)
	passwordChange=$(grep "^$login:" /etc/shadow | cut -d: -f3)
	minimumAge=$(grep "^$login:" /etc/shadow | cut -d: -f4)
	maximumAge=$(grep "^$login:" /etc/shadow | cut -d: -f5)
	warningPeriod=$(grep "^$login:" /etc/shadow | cut -d: -f6)
	inactivityPeriod=$(grep "^$login:" /etc/shadow | cut -d: -f7)
	expirationDate=$(grep "^$login:" /etc/shadow | cut -d: -f8)
	echo "LOGIN: $login"
	echo "PASSWORD: $password"
	echo "DATE OF LAST PASSWORD CHANGE: $passwordChange"
	echo "MINIMUM PASSWORD AGE: $minimumAge"
	echo "MAXIMUM PASSWORD AGE: $maximumAge"
	echo "PASSWORD WARNING PERIOD: $warningPeriod"
	echo "PASSWORD INACTIVITY PERIOD: $inactivityPeriod"
	echo "ACCOUNT EXPIRATION DATE: $expirationDate"
	return 0
}
