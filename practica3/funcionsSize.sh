#! /bin/bash 
# @edt ASIX M01-ISO 
# Curs 2019-2020 
# funcions 
# --------------------------------------------------------------------

function allfstypeif(){
}

function allfstype(){
	egrep -v "^[[:blank:]]*#|^$" /etc/fstab | tr -s '[:blank:]' ':' | sort -t: -k3,3 | cut -d: -f1,2 | tr ':' '\t'
	return 0
}

function fstype(){
	fstype=$1
	egrep -v "^[[:blank:]]*#|^$" /etc/fstab | tr -s '[:blank:]' ':' | grep "^[^:]*:[^:]*:$fstype:" | cut -d: -f1,2 | sort -t: -k1n,1
	return 0
}

function allgroupsize(){
	llistaGids=$(cut -d: -f3 /etc/group | sort -n | egrep "^[0-9]{,2}$|^100$")
	for gid in $llistaGids
	do
		gidsize $gid
	done
	return 0
}

function allgidsize(){
	llistaGids=$(cut -d: -f3 /etc/group | sort -n)
	for gid in $llistaGids
	do
		gidsize $gid
	done
	return 0
}

function gidsize(){
	if [ $# -ne 1 ]
	then
		echo "Error: numero d'args invàlid"
		echo "$0 gid"
		return 1
	fi
	gid=$1
	grep "^[^:]*:[^:]*:$gid:" /etc/group &>/dev/null
	if [ $? -ne 0 ]
	then
		echo "Error: gid $gid inexistent"
	else
		llistaLogins=$(grepgid $gid)
		for login in $llistaLogins
		do
			fsize $login
		done
	fi
	return 0
}

function grepgid(){
	if [ $# -ne 1 ]
	then
		echo "Error: numero d'args invàlid"
		echo "$0 gid"
		return 1
	fi
	gid=$1
	grep "^[^:]*:[^:]*:$gid:" /etc/group &>/dev/null
	if [ $? -ne 0 ]
	then
		echo "Error: gid $gid inexistent"
	else
		grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1
	fi
	return 0
}

function loginboth(){
	if [ $# -eq 0 ]
	then
		while read -r line
		do
			login=$line
			grep "^$login:" /etc/passwd &>/dev/null
			if [ $? -ne 0 ]
			then
				echo "Error: login $login inexistent"
			else
				fsize $login
			fi
		done
	else
		file=$1
		loginfile $file
	fi
	return 0
}

function loginfile(){
	if [ $# -ne 1 ]
	then
		echo "Error: numero d'args invàlid"
		echo "Usage: $0 loginfile"
		return 1
	fi
	file=$1
	if ! [ -f $file ]
	then
		echo "Error: $file no és un regular file"
		return 2
	fi
	llistaLogins=$(cat $file)
	for login in $llistaLogins
	do
		
		fsize $login
	done
	return 0	
}

function loginargs(){
	if [ $# -lt 1 ]
	then
		echo "Error: numero d'args invàlid"
		echo "Usage: $0 login[...]"
		return 1
	fi

	llistaLogins=$*
	for login in $llistaLogins
	do
		grep "^$login:" /etc/passwd &>/dev/null
		if [ $? -ne 0 ]
		then
			echo "Error: login $login inexistent"
		else
			fsize $login
		fi
	done
	return 0
}

function fsize(){
	login=$1
	home=$(grep "^$login:" /etc/passwd | cut -d: -f6)
	ocupacio=$(du -sh $home | tr '[:blank:]' ':' | cut -d: -f1)
	echo $login $ocupacio
	return 0
}
